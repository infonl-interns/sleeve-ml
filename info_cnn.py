from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from pattern_data import PatternData

import numpy as np
import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)


LABEL_AMOUNT = 6

def cnn_model_fn(features, labels, mode):
    input_layer = features["x"]# tf.reshape(features["x"], [-1, 3, 4, 1])

    print(input_layer)

    # conv1 = tf.layers.conv2d(
    #     inputs=input_layer,
    #     filters=32,
    #     padding="same",
    #     activation=tf.nn.rel
    # )

    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=32,
        kernel_size=1,
        padding="same",
        activation=tf.nn.relu)

    dropout = tf.layers.dropout(
            inputs=conv1, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

    logits = tf.layers.dense(inputs=dropout, units=10)

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=logits, axis=1),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def main(unused_argv):
    train_data, train_labels = PatternData.get_tensor_data()

    eval_data, eval_labels = PatternData.get_tensor_data()

    info_classifier = tf.estimator.Estimator(
        model_fn=cnn_model_fn, model_dir="/tmp/info_convnet_model")

    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        batch_size=100,
        num_epochs=None,
        shuffle=True)
    info_classifier.train(
        input_fn=train_input_fn,
        steps=5000,
        hooks=[logging_hook])

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = info_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results)


if __name__ == "__main__":
    tf.app.run()
