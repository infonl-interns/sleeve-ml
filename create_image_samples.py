"""
This script generates pattern images that can be fed to the inception model retraining script
"""

import numpy as np
import os
import random
from PIL import Image
import colorsys

IMG_DIR = os.path.dirname(os.path.abspath(__file__)) + '/img/'
AMOUNT_OF_SAMPLES = 100

patterns = {
    'square': [
        [1, 2, 3],
        [10, 0, 4],
        [9, 0, 5],
        [8, 7, 6]
    ],
    'stroke-left': [
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 0, 0]
    ],
    'l-shape': [
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 5, 6]
    ],
    'triangle': [
        [0, 0, 0],
        [0, 0, 1],
        [0, 2, 6],
        [3, 4, 5]
    ]
}

emotions = [
    "angry",
    "touch",
    "stroke"  # to pet
]

if not os.path.exists(IMG_DIR):
    os.makedirs(IMG_DIR)

for p in patterns:
    pattern = patterns[p]

    for e in emotions:
        if not os.path.exists(IMG_DIR + os.sep + p + "-" + e):
            os.makedirs(IMG_DIR + os.sep + p + "-" + e)

        if not os.path.exists(IMG_DIR + os.sep + p + "-" + e + "-reverse"):
            os.makedirs(IMG_DIR + os.sep + p + "-" + e + "-reverse")

    hue = 0 # we will leave hue on 0, no need for it
    saturation = 0 # determines order, 0 when untouched!
    value = 0 # determines intensity, 1 when untouched!

    for i in range(0, AMOUNT_OF_SAMPLES):
        y = 0
        x = 0
        data = np.zeros((4, 3, 3), dtype=np.uint8)
        reverseData = np.zeros((4, 3, 3), dtype=np.uint8)

        for e, eVal in enumerate(emotions):

            maxVal = 0

            for ri, r in enumerate(pattern):
                for ci, c in enumerate(r):
                    if c > maxVal:
                        maxVal = c

            for ri, r in enumerate(pattern):
                for ci, c in enumerate(r):
                    # e stands for intensity level (eVal for corresponding str)
                    # c stands for value in the grid ^. >= 1 means a touch, c corresponds with order too
                    # ci is the order left top to right bottom (array loop) (column)
                    # ri is the row of this loop

                    value = 255

                    if c >= 1:
                        if e == 0:
                            value = random.randint(0, 32)
                        if e == 1:  # Medium touch
                            value = random.randint(65, 128)
                        if e == 2:  # Brushy brushy
                            value = random.randint(129, 200)

                        data[ri, ci] = [value, value, c * 20]
                        reverseData[ri, ci] = [value, value, (maxVal - c) * 20]
                    else:
                        data[ri, ci] = [value, value, value] # last value 0?
                        reverseData[ri, ci] = [value, value, value]

            im = Image.fromarray(data)
            im.save(IMG_DIR + os.sep + p + "-" + eVal + os.sep + str(i) + ".jpg", format='JPEG', quality=100)

            imr = Image.fromarray(reverseData)
            imr.save(IMG_DIR + os.sep + p + "-" + eVal + "-reverse" + os.sep + str(i) + ".jpg", format='JPEG',
                     quality=100)


# function accepts values between 0.0 and 1.0 (hsv, 360, 100, 100)
def hsv_to_rgb(hue, saturation, value):
    r, g, b = colorsys.hsv_to_rgb(hue, saturation, value)

    r *= 255
    g *= 255
    b *= 255

    return r, g, b