import struct
import threading
import socket
import logging
import time
import calendar
import os
from socket import error as socket_error

PHONE_IP_ADDRESS = '10.102.1.13'
ELITAC_PORT = 50000
THRESHOLD = 1650
SESSION_DIR = os.path.dirname(os.path.abspath(__file__)) + '/test/'
DISCARD_NO_INPUT_ROWS = True

running = False
session_running = False


########################################################################################################################
# Simple fix for the scoping problem

def stop_threads():
    running = False


########################################################################################################################

class Client:

    def __init__(self, sock):
        self.sock = sock
        self.initialise_sleeve()

    def initialise_sleeve(self):
        try:
            logging.info('Trying to setup sensors')
            # init sensors ({ID},{Type},{BufferSize},{I2C_Address})
            self.sock.send(b'!SetupSensor,5,C,1024,1')
        except socket_error as e:
            logging.error('Failed to setup sensors...')

    # actuator_id (1-12), intensity(1-15?), duration in milliseconds
    def change_tactor_state(self, actuator_id, intensity, duration):
        self.sock.send(
            str.encode('!ChangeTactorState,' + str(actuator_id) + ',' + str(intensity) + ',' + str(duration)))

    # call after setting tactor state(s)
    def execute_tactor_states(self):
        self.sock.send(b'!ExecuteTactorStates')

    def request_updates(self):
        while running:
            try:
                # set data marker for ringbuffer ?
                self.sock.send(b'!SetDataMarker,5')
                # request data
                self.sock.send(b'!GetNewData,5')
                # delay
                time.sleep(0.2)
            except:
                logging.error('Connection lost')
                break


########################################################################################################################

class Receiver:

    def __init__(self, sock):
        self.sock = sock
        self.decoder = Decoder()

    # callback should follow, every other callbacks will also be handled here!
    def listen(self):
        while running:
            try:
                bytes, address = self.sock.recvfrom(1024)

                if len(bytes) == 0:
                    break

                # logging.info('Received data from client %s', address)

                self.decoder.decode_data(bytes)
            except socket_error as err:
                logging.error('Receiving data failed, connection failed?\nPress enter to quit')
                stop_threads()

                break


########################################################################################################################

class Decoder:

    def __init__(self):
        self.writer = Writer()
        self.last_session_state = False
        self.counter = -1
        self.sample = {}
        self.reset_sample()

    def decode_data(self, data):
        if session_running != self.last_session_state:
            self.last_session_state = session_running

            if session_running:
                self.writer.reset()
            else:
                self.writer.stop()

        if data[1:11].decode('unicode_escape') == 'GetNewData':
            data = data[12:]

            if len(data) != 0:
                self.handle_new_data(data)

            return

        if data[1:11].decode('unicode_escape') == 'GetSensors':
            logging.info(data[11:])

    def handle_new_data(self, data):
        # We are not recording anything
        if self.last_session_state is False:
            return

        for i in range(0, int(len(data) / 4 / 3)):
            indent = 12 * i

            counter = struct.unpack('!i', data[0 + indent:4 + indent])[0]
            c_value = struct.unpack('!i', data[4 + indent:8 + indent])[0]
            sample_time = struct.unpack('!i', data[8 + indent:12 + indent])[0]

            sensor_number = int(c_value / 4096)

            if sensor_number == 0:
                logging.info('sensor number 0, ignore')
                return

            # new sample required
            if self.counter != counter:
                self.counter = counter
                self.reset_sample()

            sensor_intensity = round(
                (pow(c_value - (sensor_number * 4096), 2) * -1 + pow(THRESHOLD, 2)) / (pow(THRESHOLD, 2) / 18) - 1
            )

            # minus 1 indicates no touch
            if sensor_intensity < -1:
                sensor_intensity = -1

            # maximum is 15 (16 might be measured now and then, error margin)
            if sensor_intensity > 15:
                sensor_intensity = 15

            self.sample[sensor_number] = sensor_intensity

            if sensor_number == 12:
                self.writer.write(self.sample, sample_time)

    def reset_sample(self):
        self.sample = {
            1: -1,
            2: -1,
            3: -1,
            4: -1,
            5: -1,
            6: -1,
            7: -1,
            8: -1,
            9: -1,
            10: -1,
            11: -1,
            12: -1
        }


########################################################################################################################

class Writer:

    def __init__(self):
        self.fs_writer = None
        self.last_sample_time = -1

    def reset(self):
        self.stop()

        self.last_sample_time = -1

        self.fs_writer = open(SESSION_DIR + 'session_' + str(int(calendar.timegm(time.gmtime()))) + '.csv', 'w')

        for i in range(1, 13):
            self.fs_writer.write('sensor ' + str(i) + ', ')

        self.fs_writer.write('sample time\n')

    def stop(self):
        if self.fs_writer is not None:
            self.fs_writer.close()

            if self.last_sample_time:
                # remove file
                pass

    def write(self, sample, sample_time):
        if self.last_sample_time == -1:
            self.last_sample_time = sample_time

        if DISCARD_NO_INPUT_ROWS:
            empty = True

            # check is there is any touched sensor
            for i in range(1, 13):
                if int(sample[i]) > 0:
                    empty = False
                    break

            # nothing was touched, cancel writing of raw
            if empty:
                return

        # write entry / row
        for i in range(1, 13):
            self.fs_writer.write(str(sample[i]) + ', ')

        self.fs_writer.write(str(sample_time - self.last_sample_time) + '\n')


########################################################################################################################

if __name__ == '__main__':
    # init logging
    logging.getLogger().setLevel(logging.DEBUG)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    try:
        sock.connect((PHONE_IP_ADDRESS, ELITAC_PORT))
    except socket_error as e:
        logging.error('Failed to connect to %s on port %s', PHONE_IP_ADDRESS, ELITAC_PORT)
        quit()

    running = True

    c = Client(sock)
    r = Receiver(sock)

    # We run the whole client further in its own class
    client_thread = threading.Thread(target=c.request_updates)
    client_thread.start()

    receiver_thread = threading.Thread(target=r.listen)
    receiver_thread.start()

    logging.info('\'s\' to start/stop logging, just enter to quit')

    while True:
        key = input('> ')

        # Toggle session, writing file
        if key == 's':
            session_running = False if session_running else True

            if session_running:
                logging.info('started logging')
            else:
                logging.info('stopped logging')

        if key == 't':
            c.change_tactor_state(1, 15, 2000)
            c.change_tactor_state(12, 15, 2000)
            c.execute_tactor_states()

        if key == '':
            break

    # Enter key was pressed without any input, shutdown nicely
    stop_threads()
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()

    client_thread.join(0)
    receiver_thread.join(0)

    logging.info('bye!')

    quit()
