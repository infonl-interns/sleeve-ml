import os
import numpy as np
from PIL import Image

SESSION_DIR = os.path.dirname(os.path.abspath(__file__)) + '/test/'

dirs = next(os.walk(SESSION_DIR))[1]

for dir in dirs:
    files = next(os.walk(SESSION_DIR + '/' + dir))[2]

    for file in files:
        if file.find('jpg') > -1:
            continue

        reader = open(SESSION_DIR + '/' + dir + '/' + file, 'r')
        content = reader.read()
        reader.close()

        contentRows = content.split('\n')

        data = np.zeros((4, 3, 3), dtype=np.uint8)

        for l in range(0, 4):
            for k in range(0, 3):
                data[l, k] = (255, 255, 255)

        i = 0
        for cR in contentRows:
            i += 1

            if i == 1:
                continue

            j = 0
            for c in cR.split(','):
                j += 1

                if j % 13 == 0:
                    continue

                v = c.strip()

                if v == '':
                    continue

                v = int(v)

                if v > -1:
                    x = 0
                    y = 0

                    # X calculation
                    if 4 < j < 9:
                        x = 1

                    if j >= 9:
                        x = 2

                    # Y calculation
                    if j == 1 or j == 5 or j == 9:
                        y = 0

                    if j == 2 or j == 6 or j == 10:
                        y = 1

                    if j == 3 or j == 7 or j == 11:
                        y = 2

                    if j == 4 or j == 8 or j == 12:
                        y = 3

                    data[y, x] = [0, 0, 0]

        im = Image.fromarray(data)
        im.save(SESSION_DIR + '/' + dir + '/' + file.replace('csv', 'jpg'))