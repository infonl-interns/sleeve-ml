package org.tensorflow.demo.distance;

/**
 * Created by thomas on 14-3-2018.
 */

public class Item {
    public String name;
    public double width;
    public double height;


    public Item(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName(){
        return name;
    }

    public double getWidth(){
        return width;
    }

    public double getHeight(){ return height; }

    public void setName(String name){this.name = name;}

    public void setWidth(double width){this.width = width;}

    public void setHeight(double height){this.height = height;}

    @Override
    public String toString(){
        return "Name: " + getName() + " width: " + getWidth() + " height: " + getHeight();
    }

}