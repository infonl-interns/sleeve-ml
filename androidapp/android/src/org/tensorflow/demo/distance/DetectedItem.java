package org.tensorflow.demo.distance;


import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thomas on 12-3-18.
 */

public class DetectedItem extends Item{

    // name of detected object and size in meters
    public String name;
    public ReferenceItem referenceItem;
    public double distance;

    private static final HashMap<String, ReferenceItem> itemMap;
    static{
        itemMap = new HashMap<String, ReferenceItem>();
        // small items
        itemMap.put("cup",          new ReferenceItem("cup", 112, 89, 0.5,"wh"));
        //itemMap.put("cell phone",   new ReferenceItem("cell phone", 120, 60, 0.5,"wh"));
        //itemMap.put("dog",          new ReferenceItem("dog",170,140,1.5,"wh"));
        //itemMap.put("backpack",     new ReferenceItem("backpack", 170,140,1.5,"wh"));
        itemMap.put("apple",        new ReferenceItem("apple", 110,110,0.5,"wh"));
        itemMap.put("teddy bear",   new ReferenceItem("teddy bear", 92,78,2,"wh"));
        itemMap.put("banana",       new ReferenceItem("banana", 160, 95, 0.5, "wh"));
        //itemMap.put("wine glass",   new ReferenceItem("wine glass", 90, 150, 0.5, "wh"));
        //itemMap.put("laptop",       new ReferenceItem("laptop",160,160,1.5,"wh"));
        itemMap.put("book",         new ReferenceItem("book", 170,190,1,"wh"));
        //itemMap.put("stop sign",    new ReferenceItem("stop sign", 100,100,1.5,"wh"));


        // medium items
        //itemMap.put("person",       new ReferenceItem("person", 200, 540,2,"w"));
        //itemMap.put("chair",        new ReferenceItem("chair", 166, 294,2, "wh"));
        //itemMap.put("refrigerator", new ReferenceItem("refrigerator", 150,300, 3, "w"));
        //itemMap.put("tv",           new ReferenceItem("tv", 300,200,2.5,"h"));
        //itemMap.put("sink",         new ReferenceItem("sink",200,200,2.5,"wh"));
        //itemMap.put("potted plant", new ReferenceItem("potted plant", 200,400,1.5,"w"));
        //itemMap.put("bicycle",      new ReferenceItem("bicycle",400,300,3,"h"));

        // large items
        //itemMap.put("car",          new ReferenceItem("car",300,200,5,"h"));

        // unknown
        itemMap.put("unknown", new ReferenceItem("unknown",0,0,0,"wh"));

    }

    public DetectedItem(String name, double width, double height){
        super(name, width, height);

        this.referenceItem = getReferenceItem(name);
        this.distance = calculateDistance(referenceItem);
    }
    // key-value pair of names and sizes
    private ReferenceItem getReferenceItem(String objectName){
        if(itemMap.get(objectName) == null){
            return itemMap.get("unknown");
        }

        return itemMap.get(objectName);
    }

    // To calculate distance, take the width and height from the reference object
    // so that proportion or relative scale will be calculated to return the estimated distance
    private double calculateDistance(ReferenceItem referenceItem){

        String orientation = referenceItem.getOrientation();
        double relativeScale = 1;
        double referenceWidth = referenceItem.getWidth();
        double referenceHeight = referenceItem.getHeight();
        double referenceDistance = referenceItem.getDistance();

        switch(orientation){
            case "w":
                relativeScale = ( width / referenceWidth );
                break;
            case "h":
                relativeScale = ( height / referenceHeight );
                break;
            case "wh":
                relativeScale = (( width / referenceWidth ) + ( height / referenceHeight)) / 2;
                break;
        }

        //Math.pow(referenceDistance * relativeScale,2)
        /*
        if(referenceItem.getName() == "cup") {
            Log.e("Relative scale: ", relativeScale + "");
            Log.e("Width ", width + "");
            Log.e("Height ", height + "");
        }
        Log.e("Distance calculated: ", " " +  referenceDistance / relativeScale);

        */
        // https://en.wikipedia.org/wiki/Inverse-square_law
        return referenceDistance / relativeScale;
    }

    public double getDistance() { return distance;}

    @Override
    public String toString(){
        return super.toString();
    }

}
