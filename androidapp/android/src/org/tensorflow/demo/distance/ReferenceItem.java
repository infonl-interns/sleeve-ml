package org.tensorflow.demo.distance;

/**
 * Created by thomas on 14-3-2018.
 */

public class ReferenceItem extends Item{
    private double distance;
    private String orientation;


    public ReferenceItem(String name, double width, double height, double distance, String orientation){
        super(name, width, height);
        this.name = name;
        this.width = width;
        this.height = height;
        this.distance = distance;
        this.orientation = orientation;
    }

    public double getDistance() { return distance; }

    public String getOrientation() { return orientation; }

    @Override
    public String toString(){
        return "Name: " + getName() + " width: " + getWidth() + " height: " + getHeight() + " distance: " + getDistance() + " orientation: " + getOrientation();
    }

}