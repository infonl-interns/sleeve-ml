package org.tensorflow.demo.communication;

/**
 * Created by thomas on 3-4-2018.
 */

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.demo.distance.Item;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Dictionary of  tactile pattern commands
 * !ActivateTactor,tactorNumber, offset, duration, intensity
 */
public class TactilePatterns {

    private static TactilePatterns instance = null;

    private static HashMap<String, ArrayList> patternMap = new HashMap();

    private String jsonString;



    private TactilePatterns() {
        // put patterns along their items
        //Log.e("Tactilepatterns", "Test: TactilePatterns() called");

    }

    private TactilePatterns(Context context){
        // read json file
        jsonString = loadJSONFromAsset(context, "patterns.json");

        Log.e("Loaded JSON string:", jsonString);

        // parse json and create list of patterns
        try {
            JSONObject obj = new JSONObject(jsonString);

            JSONArray array = obj.getJSONArray("patterns");
            for(int i = 0; i < array.length(); i++){
                JSONObject patternsObject = array.getJSONObject(i);

                String objectName = patternsObject.get("name").toString();

                // define pattern

                ArrayList<Vibration> vibrationList = new ArrayList();

                // get through the patterns

                JSONArray pattern = patternsObject.getJSONArray("pattern");
                for(int j = 0; j < pattern.length(); j++){
                    JSONObject patternObject = pattern.getJSONObject(j);

                    int motorId   = Integer.parseInt(patternObject.get("motorId").toString());
                    int intensity = Integer.parseInt(patternObject.get("intensity").toString());
                    int duration  = Integer.parseInt(patternObject.get("duration").toString());
                    int delay     = Integer.parseInt(patternObject.get("delay").toString());

                    Vibration vibration = new Vibration(motorId, intensity, duration, delay);
                    vibrationList.add(vibration);
                }

                patternMap.put(objectName, vibrationList);
            }


        } catch (JSONException e){
            // handle exception
        }
    }

    public static TactilePatterns getInstance() {
        //Log.e("Tactilepatterns","is getInstance being called?");


        if(instance == null) {
            instance = new TactilePatterns();
        }
        return instance;
    }

    // passing context for asset reading
    public static TactilePatterns getInstance(Context context) {
        Log.e("tactilepatterns", "Tactilepatterns (Context) Called!");
        if(instance == null) {
            Log.e("instance is null", "instance is nullll");
            instance = new TactilePatterns(context);
        }
        return instance;
    }


    public ArrayList<Vibration> getPattern(String name){
        return patternMap.get(name);
    }

    public boolean checkPattern(String name){
        return patternMap.containsKey(name);
    }


    public String loadJSONFromAsset(Context context, String filePath) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filePath);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
