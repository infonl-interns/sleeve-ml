package org.tensorflow.demo.communication;

import android.util.Log;

import org.tensorflow.demo.distance.DetectedItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thomas on 20-3-18.
 */


// This class is responsible for handling the detected items and respond to it (sleeve/ tts etc)
public class DetectionHandler {

    private Map<Integer, ArrayList> dItemMap = new HashMap<>();

    private CommunicationHandler cHandler = CommunicationHandler.getInstance();

    public DetectionHandler(){

    }

    public void handle(int id, DetectedItem item){

        if(dItemMap.get(id) == null){
            dItemMap.put(id, new ArrayList<DetectedItem>());
        }

        dItemMap.get(id).add(item);
    }

    public void reset() {

        // if no objects are detected, then there is not a lot to reset
        if (dItemMap.size() == 0) {
            return;
        }

        cHandler.handle(dItemMap);

        // clears the map for next detection
        dItemMap.clear();
    }
}