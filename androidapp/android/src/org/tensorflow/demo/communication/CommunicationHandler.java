package org.tensorflow.demo.communication;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.tensorflow.demo.distance.DetectedItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/** Handler that handles incoming data and prepares information for the haptic sleeve communications
 * Created by thomas on 21-3-18.
 *
*/


public class CommunicationHandler implements Runnable {

    private static CommunicationHandler instance = null;
    private SleeveManager sManager;
    private Handler handler;
    private int interval;

    DetectedItem detectedItem;


    private CommunicationHandler(){
        handler = new Handler(Looper.getMainLooper());

        sManager = new SleeveManager();

        interval = 8000;
    }

    // ugly and dirty singleton, easiest way to cancel thread through activity
    public static CommunicationHandler getInstance() {
        if(instance == null) {
            instance = new CommunicationHandler();
        }
        return instance;
    }

    /**
     * Handles the incoming data and filters it on closest distance
     * @param map of detected items
     */
    public void handle(Map<Integer, ArrayList> map){
        for (ArrayList<DetectedItem> list : map.values()) {

            if (list == null) {
                Log.e("list is null", "null");
                return;
            }

            for (DetectedItem di : list) {

                if(di.getDistance() == 0){
                    continue;
                }

                if(detectedItem == null){
                    detectedItem = di;
                }

                if(di.getDistance() < detectedItem.getDistance()){
                    detectedItem = di;
                }

            }
        }
    }

    public void setInterval(int interval){
        this.interval = interval;
    }

    public int getInterval(){
        return interval;
    }

    /**
     * Haptic sleeve communication on a specified frequency
     */
    @Override
    public void run(){
        // try and setup the sleeve, if the sleeve is already set up, continue code
        Log.e("manager running", String.valueOf(sManager.getRunning()));


        if(!sManager.getRunning()) {
            Log.e("Sleeve setup", "Trying to set up sleeve...");
            sManager.setup();
            loop();
            return;
        }

        // just logging to see whether the interval is every x seconds
        // Log.e("Interval", new SimpleDateFormat("hh:mm:ss").format(Calendar.getInstance().getTime()));


        // code for haptic sleeve should be run here
        // perhaps send item instead of name to sleeve manager?
        // can resolve the handling of objects and distances there.

        if(detectedItem != null){
            sManager.runPattern(detectedItem);
            detectedItem = null;
        }

        loop();
    }

    public void loop(){
        handler.postDelayed(this, interval);
    }

    public void pause() {
        handler.removeCallbacks(this);
    }

    public void resume(){
        handler.postDelayed(this, interval);
    }
}

