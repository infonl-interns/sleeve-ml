package org.tensorflow.demo.communication;

import android.util.Log;

import org.tensorflow.demo.BuildConfig;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by thomas on 4-4-18.
 */

public class PacketSender implements Runnable {

    private int port;
    private DatagramSocket socket;
    private InetAddress address;

    private ArrayList<Vibration> pattern;
    private double distance;

    public PacketSender(DatagramSocket socket, InetAddress address, int port, ArrayList<Vibration> pattern, double distance) {
        this.socket = socket;
        this.address = address;
        this.port = port;
        this.pattern = pattern;
        this.distance = distance;
    }

    private void sendPackets(ArrayList<Vibration> pattern, double distance) {

        for (Vibration v : pattern) {

            byte[] sleeveCommand = v.toString().trim().getBytes();
            DatagramPacket packet = new DatagramPacket(sleeveCommand,
                    sleeveCommand.length,
                    address,
                    port);

            try {

                if (socket == null) {
                    Log.e("PacketSender", "Elitac not connected");

                    // perhaps quit manager there, so it will restart?

                    return;

                }

                // set tactors in order to execute
                socket.send(packet);
                Log.e("Sent packet!!", v.toString());


                // execute haptic packet
                byte[] tactorExecute = "!ExecuteTactorStates".trim().getBytes();
                DatagramPacket executePacket = new DatagramPacket(tactorExecute,
                        tactorExecute.length,
                        address,
                        port);
                socket.send(executePacket);

                //delay for sending next packet
                int milliSeconds;
                if (v.getDelay() <= 100) {
                    milliSeconds = 100;
                } else {
                    milliSeconds = v.getDelay() - 100;
                }

                // sleep for x milliseconds to delay sending of next packet
                //Log.e("thread", "sleeping for: " + milliSeconds);
                Thread.sleep(milliSeconds);
                //Log.e("thread", "finished sleeping " + milliSeconds);

                byte[] tactorClear = "!ClearTactorStates".trim().getBytes();
                DatagramPacket clearTactors = new DatagramPacket(tactorClear,
                        tactorClear.length,
                        address,
                        port);
                socket.send(clearTactors);

                // the reason the delay is set, or else the next packet will be sent immediately
                // and is then not obtained by Elitac sleeve
                Thread.sleep(100);


            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // after vibrations, vibrate distance

        // /*

        try {
            if (socket == null) {
                Log.e("PacketSender", "Elitac not connected, can't send distance");

                // perhaps quit manager there, so it will restart?

                return;
            }

            // small relaxation moment before sending distance packets
            Thread.sleep(800);


            int count = 0;

            distance = Math.random() * 5;

            if (distance <= 0) {
                // kapot, zou bijzonder zijn
                Log.e("PacketSender", "distance under or equal to zero");

            } else if (distance > 0 && distance <= 1) {
                count = 1;
            } else if (distance > 1 && distance <= 2) {
                count = 2;
            } else if (distance > 2 && distance <= 4) {
                count = 3;
            } else {
                // distance > 4 meters
                count = 4;
            }

            Log.e("PacketSender", "Send " + count + " vibrations, distance: " + distance);

            for (int i = 0; i < count; i++) {

                byte[] distanceCommand = "!ChangeTactorState,5,15,300".trim().getBytes();

                DatagramPacket packet = new DatagramPacket(distanceCommand,
                        distanceCommand.length,
                        address,
                        port);

                socket.send(packet);

                // execute haptic packet
                byte[] tactorExecute = "!ExecuteTactorStates".trim().getBytes();
                DatagramPacket executePacket = new DatagramPacket(tactorExecute,
                        tactorExecute.length,
                        address,
                        port);
                socket.send(executePacket);

                // sleep for 500ms before sending next packet
                Thread.sleep(400);

                byte[] tactorClear = "!ClearTactorStates".trim().getBytes();
                DatagramPacket clearTactors = new DatagramPacket(tactorClear,
                        tactorClear.length,
                        address,
                        port);
                socket.send(clearTactors);


                // the reason the delay is set, or else the next packet will be sent immediately
                // and is then not obtained by Elitac sleeve
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // */

    }

    @Override
    public void run() {
        sendPackets(pattern, distance);
    }
}
