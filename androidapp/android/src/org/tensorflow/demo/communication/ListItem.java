package org.tensorflow.demo.communication;

import org.tensorflow.demo.distance.DetectedItem;

/**
 * Created by thomas on 20-3-18.
 */

public class ListItem {

    private int id;
    private DetectedItem item;

    public ListItem(int id, DetectedItem item) {

        this.id   = id;
        this.item = item;
    }

    public int getId(){return id;}
    public DetectedItem getItem () { return item;}

}
