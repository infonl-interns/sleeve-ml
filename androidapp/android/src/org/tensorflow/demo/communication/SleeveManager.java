package org.tensorflow.demo.communication;

import android.util.Log;

import org.tensorflow.demo.distance.DetectedItem;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by thomas on 28-3-18.
 */

public class SleeveManager implements Runnable{

    private final int ELITAC_PORT = 50000;
    private InetAddress address;
    private DatagramSocket socket;

    private boolean running;

    private String setupMessage = "?IsConnected";

    TactilePatterns tactilePatterns = TactilePatterns.getInstance();

    public SleeveManager(){

    }

    public void setup() {
        // before running set up the sleeve
        if(!running){
            Thread thread = new Thread(this);
            thread.start();
        }
    }


    public void runPattern(DetectedItem item){
        String itemName = item.getName();

        if(tactilePatterns.checkPattern(itemName) == true){


            //Log.e("Item size: ", "height: " + item.getHeight() + " width: " + item.getWidth());
            Log.e("Executing pattern: ", itemName + ", distance: " + item.getDistance());


            Runnable r = new PacketSender(socket, address, ELITAC_PORT, tactilePatterns.getPattern(itemName), item.getDistance());
            new Thread(r).start();
        } else {
            Log.e("SleeveManager", "Does not contain pattern [" + itemName + "], not executing pattern.");
        }
    }

     public boolean getRunning(){
        return running;
    }

    public void close() {
        socket.close();
    }

    @Override
    public void run() {

        // only for demo if you have no elitac
        // needs change
        //if(BuildConfig.DEBUG){
        //  running = true;
        //}

        // try and set up socket
        if(!running){
            try {

                if(socket == null) {
                    address = InetAddress.getLocalHost();
                    socket = new DatagramSocket();
                    socket.setReuseAddress(true);
                    //socket.bind(new InetSocketAddress(ELITAC_PORT));
                }

            } catch (UnknownHostException e) {
                Log.e("unknownhostexception",String.valueOf(socket) );

                e.printStackTrace();
            } catch (SocketException e){
                Log.e("socket",String.valueOf(socket.getPort()) );

                e.printStackTrace();
            }

            byte[] message = setupMessage.trim().getBytes();
            DatagramPacket setupPacket = new DatagramPacket(message, message.length, address, ELITAC_PORT);

            try {
                socket.send(setupPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        while(!running){
            byte[] data = new byte[2048];
            DatagramPacket receivedPacket = new DatagramPacket(data, data.length);
            try {
                socket.receive(receivedPacket);
                String packetData = new String(receivedPacket.getData(), receivedPacket.getOffset(), receivedPacket.getLength());
                Log.e("Packet received!", packetData);
                Log.e("Connected to sleeve: ", String.valueOf(packetData.equals("IsConnected,true")));

                if(packetData.equals("IsConnected,true")) {
                    running = true;
                }
            } catch(IOException e) {
                Log.e("No packet received!", "help");
            }
        }

    }
}
