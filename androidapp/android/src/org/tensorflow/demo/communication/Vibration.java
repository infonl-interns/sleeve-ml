package org.tensorflow.demo.communication;

/**
 * Created by thomas on 23-4-18.
 */

public class Vibration {

    private int motorId;
    private int intensity;
    private int duration;
    private int delay;

    public Vibration(int motorId, int intensity, int duration, int delay){
        this.motorId = motorId;
        this.intensity = intensity;
        this.duration = duration;
        this.delay = delay;
    }


    public int getDelay(){
        return this.delay;
    }

    @Override
    public String toString(){
        return "!ChangeTactorState," + motorId + "," + intensity + "," + duration;
    }
}
