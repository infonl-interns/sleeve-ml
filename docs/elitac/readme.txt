De TASST-sleeve bestaat uit 12 sensoren en 12 tactoren, verbonden met een controlemodule waarmee op afstand via Bluetooth verbonden kan worden..
Installeer de "ElitacConnect" app op de smartphone.
Pair de TASST-sleeve met de smarphone.
Hoe de controlemodule en de tactoren aan te sturen vanuit je eigen applicatie via UDP lees de "Elitac Tactile Display Manual".
De 12 TASST-sensoren kunnen als 1 sensor van het type 'C' uitgelezen worden via UDP (zie "UDP Communicatie"). De uitgelezen waarden zijn 16 bit. De 4 hoogste bitjes maken het sensornummer van de TASST-sleeve. De onderste 12 bits de ruwe ADC-waarde (hoge waarden representeren meer druk op de sensor, lagere waarden minder). Hieronder een stukje voorbeeld code om dit te verkrijgen:
 
byte [] subArray2 = Arrays.copyOfRange(incomming, dataCounter, incomming.length);                               
// Get all the usefull data from the byte stream.
for(int i = 0; i < subArray2.length; i = i + 12){  // 3 values, 4 bytes per value.
       int counter = ByteBuffer.wrap(Arrays.copyOfRange(subArray2, 0 + i, 4 + i)).getInt();              // 4 bytes counter (as Integer)
       int Cvalue = ByteBuffer.wrap(Arrays.copyOfRange(subArray2, 4 + i, 8 + i)).getInt();        // 4 bytes Cvalue (as Integer)
       int sampleTime = ByteBuffer.wrap(Arrays.copyOfRange(subArray2, 8 + i, 12 + i)).getInt();   // 4 bytes sampleTime (as Integer)
                                          
       int sensorNumber = Cvalue / 4096;
       int sensorData = Cvalue - (sensorNumber * 4096);
       System.out.println("SensorNumber: " + sensorNumber + " sensorData: " + sensorData + " counter: " +counter +" [raw:" + Cvalue +"]");
}


