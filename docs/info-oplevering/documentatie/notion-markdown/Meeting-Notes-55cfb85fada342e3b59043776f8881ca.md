# Meeting Notes 📝

Keep a history of your meetings.

---

Tip: to configure the "+ New Meeting" button, right-click and click "Configure", then modify or drag in new blocks.