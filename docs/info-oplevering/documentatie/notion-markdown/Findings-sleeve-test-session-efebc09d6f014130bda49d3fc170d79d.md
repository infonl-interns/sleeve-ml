# Findings sleeve test session

On this page the findings for the sleeve session are documented, that Wolf Vos and Guido Baars did on November the 28th of 2016 at [Info.nl](http://info.nl) headquarters basement.

## Goal

To find relevant problems to overcome when acquiring and modifying data from the Tasst sleeve, and to get a better understanding of the limitations and possible solutions to improve this product.

### Findings list, the sleeve:

- is uncalibrated
- calibration should be done in different positions
- has delay between sender - receiver, _sometimes_ 
- writes ~8 entries per second to the database 
- writes and sends everything, including the packets without touch registrations
- uses the UDP protocol for data transmission
- not all sensors register always, some are skipped _sometimes_ 

### How we've conducted our session

We first created a bash script to read the database entries from the database with 1 second interval. The output was formatted to fit in a terminal window. 

 `[data-query.sh](http://data-query.sh)` 

	#/bin/bash
	mysql -h 146.185.157.31 -P 3306 --user="sleeveuser" --password="sleeveuserpw" --execute="SELECT * FROM sleevedatabase.sample WHERE userid = 'c4a1d361836f89b2' ORDER BY id DESC LIMIT 0,60;" --skip-column-names

 `[data-watcher.sh](http://data-watcher.sh)` 

	#/bin/bash
	watch -n 1 ./data-query.sh

What the `data-query` does is a one-time command to output the data. The `data-watcher` runs this script with the speed -n 1, which means 1 time per second. The script is started in a bash-shell environment with mysql (client) installed, with the following command:

	./data-watcher.sh

### Findings explained

 `**Uncalibrated**` 

When the user puts the sleeve around their arm, the sleeve starts transmitting data. It recognizes the action as normal touches, resulting in a faulty data set and faulty transmissions of touches. Behavior that suits more in this context is that the sleeve doesn't transmit, or the data transmitted is dropped until the sleeve has been calibrated. This can be achieved in multiple ways, but in the scope of the project the way to achieve this (trough the middleware) is limited.

Possible solutions that could work for this situation is:

- Touch gesture or tapping used as a command to indicate a calibration. Same goes for ending the calibration.
- Touch gesture or tapping used as a command to stop the sending and packets, and the same for start sending. This would rule out generating erratic data while putting the sleeve on.
- A smart combination of those combined in a sequence.

 **`Calibration positions`** 

When the user has the sleeve around their arm, the calibration of a default value for starting to recognize pressure on a sensor has to be done for multiple positions of the arm. When you turn your arm with hand palms up from a hand palms down facing position, the data changes accordingly, recognizing non-existent touches. This means that the 0-point for recognizing a touch depends on the arm position. Possible solutions to overcome this problem:

- Use the highest detected position of the sensor as a stable 0-point.
- Making use of an accelerometer to detect the arm position.

 `**Delay**` 

We've found the delay occurred in the communication between the tasst-app and the closed source manufacturer application on localhost. The tasst application has a on-screen input method to use the connection without sleeve. In the same testing setting we've found that the signal was delayed by 1 - 6 seconds when the real sleeve was used, and with the virtual input method almost no delay was detected. This means the delay has to take place between the the 2 Android apps or the Bluetooth connection between the manufacturer app and the sleeve. For this problem there is not a feasible solution, nor proper way to debug what causes this as long as there is no SDK available.

 **`Samples per second`** 

The samples recorded per second is ~8, where recorded means written to the database. This estimation is based on the SQL timestamp written to every entry by the database.

 **`UDP and missing packets`** 

UDP is probably not the best choice for reliable data transmission. It would be for large volumes of data, but due to the relatively small information footprint the packets contain this would better be avoided. Some packets seem to be missing from the datasets. This is probably caused by the use of UDP for transmission. However, the choice to switch to another protocol like TCP for example is not a viable option as long as the closed source application of the manufacturer uses UDP to do the initial transmission after the data has been received trough Bluetooth connection.

### Data point layout