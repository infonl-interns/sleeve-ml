![](https://s3-us-west-2.amazonaws.com/notion-static/d741e222ebc5414d9544c1d109e7abff/infonl-logo-no-payoff.png)

# Info NL Home

All of your team's knowledge lives here.

 **Projectmanager:** 

## Today

---

[Meeting Notes 📝](./Meeting-Notes-55cfb85fada342e3b59043776f8881ca.md)

[Acceptance environment](./Acceptance-environment-23c989cdb2b24c1c9e331a84bec61550.md)

[Architecture](./Architecture-e79a322d1e144a5aa2ac3c8f20edc21d.md)

[Signal Smoothing](./Signal-Smoothing-24fc58d2758244feb4177521b5b88ad8.md)

[Findings sleeve test session](./Findings-sleeve-test-session-efebc09d6f014130bda49d3fc170d79d.md)

[API endpoints](./API-endpoints-8e3f266ef1d94228a95a1447f4b2ccd0.md)

[Sleeve telefoon](./Sleeve-telefoon-5c686c5e89f1444fb63b999c4785dd3b.md)