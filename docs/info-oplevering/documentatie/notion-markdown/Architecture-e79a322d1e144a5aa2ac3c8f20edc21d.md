# Architecture

Discussion about architectural decisions
Wednesday 30th November

Different starting points:

- Seperate application
- Extension of middleware server

Tools to use:

- Highcharts
	- Accepts Json as input format for data
-  [sbt](http://www.scala-sbt.org/) : Scala build tool
-  [Spray](http://spray.io) : an open-source toolkit for building REST/HTTP-based integration layers on top of Scala and Akka

Calculations on data:

- TBA

## API endpoints

-  **retrieve** : Retrieve data from the database, the parameters will instruct how the data is modified by the postprocessing module.

Parameters:
	-  **receiverAddress** : Filtering the data based on receiver address
	-  **senderAddress** : Filtering the data based on sender address
	-  **packetID** : Offset with value for x most recent entries (based on packet ID)
	-  **time** : Offset with value for x most recent entries (based on time)

	The parameters of this API might need some adjustment later on.

-  **info** : Show info about the data stored in DB

	Parameters (absence of value=true is interpreted as false):

	-  **totalRows** : the total amount of rows
	-  **packet{Start, End}Range** : the starting packet id in db and end packet id
	-  **dateTimeRange** : the starting date-time and ending date-time of the stored data

## Compiling

- Using SBT for builds
- Using [https://github.com/earldouglas/xsbt-web-plugin](https://github.com/earldouglas/xsbt-web-plugin) for creating .war files with SBT
- Using Tomcat for running .war web projecs

### Project setup

- scala-tasst-middleware is a JavaApp project setup. This means it is not meant to run as a JavaServerApplication or as servlet container. To refactor the current code into a more server-like application for creating an http API would cost too much time. See choices how we deal with this.

### Choices

- Refactoring UDP to TCP is not an option due to the connectable app, this would however be necessary to get reliable data in the future... To change this, the connectable app, the tasst-android app and the middleware need to be refactored. This is not in the scope of the project and can only be done when the source (connectable app) does TCP over [localhost](http://localhost) instead of UDP or delivers an SDK to connect in a more native way to the connectable IOT device.
- The API servlet application is seperate from the scala-middleware. The project setup will be best practice and can run in a J2EE (servlet) container. The scala-tast-middleware could be integrated in this servlet container, but for now this is out of scope.
- The API servlet container will get an UDP interface to which the Android-App can connect to. This should be there to make data normalization possible. After data is normalized the UDP stream will continue to the existing scala-tasst-middleware, which will write the normalized data to the DB and send the stream to the right receiving Android app. To make this setup possible, the tasst-android-app needs to use preferably a hostname, in order to change the receiving middleware to another IP address by changing the DNS record for the future. This way the IP is no longer hardcoded in the android application and can be managed from outside without recompiling or reinstalling.
- The Dashing/3DJS dashboard will be in the static folder of the container. The webserver will give these files to the clients browser to be displayed.

## Architecture visualized

![](https://s3-us-west-2.amazonaws.com/notion-static/1d4be9c5181949a3a0260fe158e77b76/architecture3.jpg)