# Signal Smoothing

The aim of the research is to find suitable smoothing techniques for the TASST signals.

## Dynamic threshold

In order to provide a good experience the motors should not be activated when the sleeve is put on. The threshold should be set dynamically, this can be done by calculating a running average. If the value is near the average it should be omitted. This can be done in a real-time fashion.

---

### Gaussian smoothing

In order to get better representation of the data at the receiving end, data can be smoothed by a Gaussian kernel. The idea is to calculate the mean and the deviation of the data sensors from the sending end and redistribute the activations over de motors of the receiving end in a Gaussian way.

---

### Movement Momentum

During the transfer of UDP packages from sender to receiver some packages might be lost, or received in a wrong order. In the case of a moving touch, momentum can be calculated. This can be useful when a package is received in the wrong order or not at all. The velocity of the previous time steps will be saved and used to predict the next activation if a UDP package is missing, if the packages are received in the right order momentum will not be used. In order to do this we first need Gaussian smoothing.

---

### Anti-aliasing

Anti-aliasing is a method to smooth the data during the process. If we combine momentum, Gaussian smoothing and anti-aliasing we can create a signal that feels smoother, it will function in our case as a smoothing over time and space. The smoothing over space will be done by a gaussian kernel. The smoothing over time will probably be an running average with a short memory.

---

### Phantom Haptics

We will also look into the possibilities of phantom haptics, by using the algorithm described in ‘Tactile Brush: Drawing on Skin with a Tactile Grid Display (Ali Israr and Ivan Poupyrev)’.

[Israr, Poupyrev - 2011 - Tactile Brush Drawing on Skin with a Tactile Grid Display.pdf](https://s3-us-west-2.amazonaws.com/notion-static/76090c2028a243fba339b514ca8a1c97/Israr_Poupyrev_-_2011_-_Tactile_Brush_Drawing_on_Skin_with_a_Tactile_Grid_Display.pdf)

---

[Seo - 2010 - Initial study for creating linearly moving vibrotactile sensation on mobile device.pdf](https://s3-us-west-2.amazonaws.com/notion-static/21eb1c339f4e47d2b96de404686fb756/Seo_-_2010_-_Initial_study_for_creating_linearly_moving_vibrotactile_sensation_on_mobile_device.pdf)

[Alles (1970) - Information Transmission by Phantom Sensations.pdf](https://s3-us-west-2.amazonaws.com/notion-static/7321238c9f15475baafd112153fcf117/Alles_(1970)_-_Information_Transmission_by_Phantom_Sensations.pdf)