# Acceptance environment

## For product owners

### How do I view the current deployment of the project?

- Visit the staging server frontend: [http://tasst.staging1.bit-students.com/](http://tasst.staging1.bit-students.com/) 

### How do I view the stories for this project?

- Visit the pivotal tracker: [https://www.pivotaltracker.com/n/projects/1909655](https://www.pivotaltracker.com/n/projects/1909655) 

## For developers

### Developing with Jenkins

- To start the application, run 'sbt compile' in the root of the repository.
- To run the application, run 'sbt run' in the root of the repostitory.
	- The application will provide feedback via stdout.

### Workflow

- Start a feature with Git-flow ( [Git-flow explained](https://www.notion.so/Git-flow-044736425acd4d5d9a6522f27b5e5fee) ).
- Push the feature to its own branch on origin (Bitbucket)
- Create a pull request via Bitbucket
- After approval, Jenkins will deploy the feature to the staging server

### Sending and receiving packets to the application

- Download PacketSender: [https://packetsender.com/download](https://packetsender.com/download) 
- To receive an ACK, use the following example ASCII input:

		{"intensityAtTouchPoint":[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,7],"sampleId":123,"userId":"ABC_DEF_GHI", "recipientId":"JKL_MNO_PQR"}

- Select **9876 ** as port, **UDP ** as protocol, and address as either [localhost](http://localhost) (9876) or [tasst.staging1.bit-students.com](http://tasst.staging1.bit-students.com) 
	- If the host [tasst.staging1.bit-students.com](http://tasst.staging1.bit-students.com) is unavailable (no DNS entry), then add this to your hosts file:

			138.68.69.115	 [tasst.staging1.bit-students.com](http://tasst.staging1.bit-students.com) 

- If you now click 'Send', you should receive an ACK packet from the application server.