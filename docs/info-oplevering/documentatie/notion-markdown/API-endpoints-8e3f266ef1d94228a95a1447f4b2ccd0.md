# API endpoints

This page describes the API endpoints of the scala-api / scala-middleware project that connects the sleeves over the internet and makes the data available to an data visualization dashboard.

 **_Endpoint: _** `/retrieve` 

	Method: 			GET
	Parameters:		receiverAddress
								senderAddress
	 packetID
	 time
	 libraryFunction
	 libraryParameter
	 lastX
	Returns:			SleeveSample (json, list)

Param: `receiverAddress` 

	Optional: yes
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI

Param: `senderAddress` 

	Optional: yes
	Accepted type: String
	Example:
	senderAddress = ABC_DEF_GHI

Param: `packetID` 

	Optional: yes
	Accepted type: Number (as Int)
	Example:
	packetID = 80000

Param: `packetIDEnd` 

	Optional: yes
	Accepted type: Number (as Int)
	Example:
	packetIDEnd = 85000

Note: use `packetIDEnd` in combination with `packetID` to retrieve a range of items between the two values.

Param: `time` 

	Optional: yes
	Accepted type: DateTime (as String)
	Example:
	time = 2016-05-23 07:01:01

Param: `libraryFunction` 

	Optional: yes
	Accepted type: String
	Example:
	libraryFunction = functionName

Param: `libraryParameter` 

	Optional: yes (part of the libraryFunction argument)
	Accepted type: Int
	Example:
	libraryParameter = 15

 _Note about library parameters: depending on the function used, a parameter might be required. For instance, the function _ `_multiply_` _ requires a parameter._ 

Param: `includeRaw` 

	Optional: yes (part of the libraryFunction argument)
	Accepted type: Boolean
	Example:
	includeRaw = true

 _Note about includeRaw: provides the raw data alongside the modified data, if a libraryFunction is applied. Cannot be called without applying a libraryFunction._ 

Param: `lastX` 

	Optional: yes
	Accepted type: Int
	Example:
	lastX = 10

 _Note: lastX does pagination on the returned results, before the pagination is done, the results are sorted on sampleid (=packetID) in descending order._ 

 **_Endpoint: _** `/info` 

	Method:				GET
	Parameters:		None
	Returns:			totalrows, packetID range, time range			

 _Note: Originally in the _ _[architecture](https://www.notion.so/Architecture-e79a322d1e144a5aa2ac3c8f20edc21d#fed63b03dd1a44859b80e005598bb81f)_ _, we defined parameters. Which seem useless, so these are omitted._ 

 **_Endpoint: _** `/functions` 

	Method:				GET
	Parameters:		None
	Returns:			LibraryFunctions (json, list)			

 **_Endpoint: _** `/threshold` (GET)

	Method:				GET
	Parameters:		receiverAddress, senderAddress, time, thresholdId
	Returns:			ThresholdList (json, list)

Param: `receiverAddress` 

	Optional: yes
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI

Param: `senderAddress` 

	Optional: yes
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI

Param: `time` 

	Optional: yes
	Accepted type: DateTime (as String)
	Example:
	time = 2016-05-23 07:01:01

Param: `thresholdId` 

	Optional: yes
	Accepted type: Int
	Example:
	thresholdId = 1

 **_Endpoint: _** `/threshold` (DELETE)

	Method:				DELETE
	Parameters:		thresholdId
	Returns:			thresholdId

Param: `thresholdId` 

	Optional: yes
	Accepted type: Int
	Example:
	thresholdId = 1

 **_Endpoint: _** `/threshold` (POST)

	Method:				POST
	Parameters:		receiverAddress, senderAddress, thresholdId, thresholdAtTouchPoint{1-12}
	Returns:			String containing GET-url for created threshold

Param: `receiverAddress` 

	Optional: yes
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI

Param: `senderAddress` 

	Optional: yes
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI

Params: `thresholdAtTouchPoint1` - `thresholdAtTouchPoint12` 

	Optional: no
	Accepted type: Int
	Examples:
	thresholdAtTouchPoint1 = 5
	thresholdAtTouchPoint2 = 3
	etc.

 _Note: The thresholdAtTouchPoint params are multiple parameters from 1 to 12._ 

 **_Endpoint: _** `/threshold/auto` (POST)

	Method:				POST
	Parameters:		senderAddress
	Returns:			String containing GET-url for created threshold

[Threshold setting](./Threshold-setting-e3b6239598d94c9895fd584eeb5b7312.md)

Param: `senderAddress` 

	Optional: no
	Accepted type: String
	Example:
	receiverAddress = ABC_DEF_GHI