# Threshold setting

Information about dynamic threshold setting

When using the TASST sleeve a threshold can be dynamically determined when using the dashboard. This can be done by pressing the New Threshold button. The system will wait for five seconds and then use the previous 200 (assuming that the sleeve sends packages with 50Hz) sleevesamples to calculate a threshold.

In order to obtain the best results follow these steps:

1. Turn on sleeve and connect to the middleware
2. Put on sleeve and hold you arm in a resting position
3. Press the 'New Threshold' button
4. The system will now listen for 5 seconds and calculate the threshold
5. When the system is done a result message will be shown and the new threshold will be applied to future signals