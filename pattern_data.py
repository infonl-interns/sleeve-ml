"""

list .copy() has been used, therefore python 3.3+ is required OR adjust the code yourself
deep copies use copy.deepcopy()

code written by Yoni Slot for info.nl 2018

Data generation functions are limited to just the patterns on 1 intensity scale (angry)
This due to the shortcomings of KMeans not being able to pass the first test and thus further experimenting
is not required

"""

import random
import numpy as np
from PIL import Image
import colorsys
import os
import copy

IMG_DIR = os.path.dirname(os.path.abspath(__file__)) + '/img/'

patterns = [
    [  # double-swipe-center
        [0, 0, 0],
        [1, 3, 5],
        [2, 4, 6],
        [0, 0, 0]
    ],
    [  # stroke-right-from-base
        [0, 0, 4],
        [0, 0, 3],
        [0, 0, 2],
        [0, 0, 1]
    ],
    [  # square
        [1, 2, 3],
        [10, 0, 4],
        [9, 0, 5],
        [8, 7, 6]
    ],
    [  # stroke-left
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 0, 0]
    ],
    [  # l-shape
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 5, 6]
    ],
    [  # triangle
        [0, 0, 0],
        [0, 0, 1],
        [0, 2, 6],
        [3, 4, 5]
    ]
]

emotions = [
    "angry",
    "touch",
    "stroke"  # to pet
]

actual_labels = [
    'double-swipe-center',
    'stroke-right-from-base',
    'square',
    'stroke-left',
    'l-shape',
    'triangle'
]


class PatternData:

    @staticmethod
    def get_all_patterns():
        return patterns

    @staticmethod
    def get_tensor_data(amount=100):
        d = []
        y = []

        for pattern_index, pattern in enumerate(patterns):
            for x in range(0, amount):
                d.append(copy.deepcopy(pattern))
                y.append(pattern_index)

        return np.array(d, dtype=np.float32), np.array(y, dtype=np.int32)

    # Returns the pattern array corresponding with the given label (str)
    @staticmethod
    def get_pattern(name):
        if name in actual_labels:
            d = []

            for ri, r in enumerate(patterns[actual_labels.index(name)]):
                for ci, c in enumerate(r):
                    "using intense touch (13,15)"
                    if c >= 1:
                        d.append([ri * 3 + (ci + 1), c, random.randint(13, 15)])

            if len(d) == 0:
                return False

            return d
        else:
            return False

    # Used to generate data based on provided patterns (distinct or full)
    @staticmethod
    def generate_pattern_data(pats, amount=100):
        d = []
        y = []

        i = 0
        for pattern in patterns:

            for x in range(0, amount):
                for ri, r in enumerate(pattern):
                    for ci, c in enumerate(r):

                        "using intense touch (13,15)"
                        if c >= 1:
                            d.append([ri * 3 + (ci + 1), c, random.randint(13, 15), i])
                            y.append(i)

            i += 1

        return np.array(d), y, actual_labels

    # Returns data, distinct labels
    @staticmethod
    def create_data():
        return PatternData.generate_pattern_data(patterns)

    @staticmethod
    def create_distinct_data():
        d_patterns = copy.deepcopy(patterns)

        pattern_index = 0
        for pattern in d_patterns:
            for ri, r in enumerate(pattern):
                for ci, c in enumerate(r):
                    if c == 0:
                        continue

                    sub_pattern_index = 0

                    for sub_pattern in d_patterns:
                        if sub_pattern_index == pattern_index:
                            sub_pattern_index += 1
                            continue

                        # logic
                        if d_patterns[sub_pattern_index][ri][ci] == c:
                            d_patterns[pattern_index][ri][ci] = 0
                            d_patterns[sub_pattern_index][ri][ci] = 0

                        sub_pattern_index += 1

            pattern_index += 1

        # distinct pattern is now stored in d_patterns

        return PatternData.generate_pattern_data(d_patterns)

    @staticmethod
    def generate_images(amount=1):
        # copy to work with
        data = copy.deepcopy(patterns)

        pattern_index = 0
        rgb_image = np.zeros((4, 3, 3), dtype=np.uint8)
        reverse_rgb_image = rgb_image.copy()

        for pattern in data:
            # Clean canvas
            image_data = np.zeros((4, 3, 3))

            # create all possible dirs
            for e in emotions:
                if not os.path.exists(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + e):
                    os.makedirs(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + e)

                if not os.path.exists(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + e + "-reverse"):
                    os.makedirs(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + e + "-reverse")

            max_val = 0
            # Set white spaces and max_val
            for i in range(0, 4):
                for y in range(0, 3):
                    if pattern[i][y] > max_val:
                        max_val = pattern[i][y]

                    image_data[i][y] = [0.0, 0.0, 1.0]

            reverse_image_data = image_data.copy()

            for a in range(0, amount):
                for emotion_index, emotion_label in enumerate(emotions):
                    for ri, r in enumerate(pattern):
                        for ci, c in enumerate(r):
                            if c == 0:
                                continue

                            """
                            image_data[ri][ci] = [0,0,0 + (c * (1.0/12.0))]
                            reverse_image_data[ri][ci] = [0, 0, 0]

                            continue

                            emotion_value = random.uniform(0.8, 1.0)
                            """
                            if emotion_index > 0:
                                if emotion_index == 1:  # touch
                                    emotion_value = random.uniform(0.6, 0.79)

                                if emotion_index == 2:  # stroke
                                    emotion_value = random.uniform(0.4, 0.59)

                            image_data[ri][ci] = [(c - 1) * (1.0 / 12.0), emotion_value, 1.0]

                            rv = (max_val - 1 - (c - 1)) * (1.0 / 12.0)
                            reverse_image_data[ri][ci] = [rv, emotion_value, 1.0]

                    for i in range(0, 4):
                        for y in range(0, 3):
                            r, g, b = hsv_to_rgb(image_data[i][y][0], image_data[i][y][1], image_data[i][y][2])
                            rgb_image[i][y] = [r, g, b]

                            r, g, b = hsv_to_rgb(reverse_image_data[i][y][0], reverse_image_data[i][y][1],
                                                 reverse_image_data[i][y][2])
                            reverse_rgb_image[i][y] = [r, g, b]

                    im = Image.fromarray(rgb_image)
                    im = im.resize((240, 320))
                    im.save(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + emotion_label + os.sep + str(
                       a) + ".jpg", format="JPEG", quality=100)

                    #im.save(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + emotion_label + ".jpg",
                    # format="JPEG", quality=100)

                    imr = Image.fromarray(reverse_rgb_image)
                    imr = imr.resize((240, 320))
                    imr.save(IMG_DIR + os.sep + actual_labels[
                        pattern_index] + "-" + emotion_label + "-reverse" + os.sep + str(a) + ".jpg", format="JPEG",
                             quality=100)

                    #imr.save(IMG_DIR + os.sep + actual_labels[pattern_index] + "-" + emotion_label + "-reverse.jpg",
                    #        format="JPEG", quality=100)

            pattern_index += 1

    @staticmethod
    def generate_mnist_like_data(amount=100, noise=False):
        d = []
        y = []

        mnist_patterns = copy.deepcopy(patterns)

        for mnist_pattern in mnist_patterns:
            for ri, r in enumerate(mnist_pattern):
                r.append(0)

        label_index = 0
        for pattern in mnist_patterns:
            for e_index, emotion in enumerate(emotions):
                for i in range(0, amount):
                    pd = np.zeros((28, 28), dtype=np.float32)
                    noise_amount = 0

                    for ri, r in enumerate(pattern):
                        for ci, c in enumerate(r):
                            val = 0.0
                            min = 0.0
                            max = 0.0

                            if e_index == 0:
                                min = 0.8
                                max = 1.0

                            if e_index == 1:
                                min = 0.6
                                max = 0.79

                            if e_index == 2:
                                min = 0.4
                                max = 0.59

                            # Chance on 4 missing pixels
                            if noise and noise_amount < 4 and random.randint(0, 1) == 1:
                                min = 0.0
                                max = 0.0
                                noise_amount += 1

                            start_row = ri * 7
                            start_col = ci * 7

                            for row in range(0, 7):
                                for col in range(0, 7):
                                    if c > 0:
                                        val = random.uniform(min, max)

                                    pd[start_row + row][start_col + col] = val

                    pdd = []
                    for ri, r in enumerate(pd):
                        for ci, c in enumerate(r):
                            pdd.append(c)

                    d.append(np.asarray(pdd, dtype=np.float32))
                    y.append(label_index)

                label_index += 1

        return np.array(d, dtype=np.float32), np.asarray(y, dtype=np.int32)


# function accepts values between 0.0 and 1.0 (hsv, 360, 100, 100)
def hsv_to_rgb(hue, saturation, value):
    r, g, b = colorsys.hsv_to_rgb(hue, saturation, value)

    r *= 255
    g *= 255
    b *= 255

    return r, g, b
