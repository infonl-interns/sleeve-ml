import numpy as np
import time
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
# Though the following import is not directly being used, it is required
# for 3D projection to work
from mpl_toolkits.mplot3d import Axes3D

from pattern_data import PatternData

# Keyboard smash
np.random.seed(123112)

# Used data
data, y, labels = PatternData.create_distinct_data()

# Clusters should equal the amount of patterns
pattern_estimator = KMeans(n_clusters=len(labels), max_iter=100000)

# Figsize is the visual size, no need to adjust
pattern_fig = plt.figure(figsize=(4, 3))

# Just visual values, camera angle on plot, no need to adjust
ax = Axes3D(pattern_fig, elev=48, azim=134)

# Timing the fitting / training
start = time.time()

# Passing label ids
pattern_estimator.fit(data[:, :3], data[:, 3])

# Resulting time
print("It took " + str((time.time() - start) * 100.0) + "ms to train/fit the kmean model")

ax.scatter(data[:, 0], data[:, 1], data[:, 2],
           c=pattern_estimator.labels_.astype(np.float), edgecolor='k')

ax.w_xaxis.set_ticklabels([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
ax.w_yaxis.set_ticklabels([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])  # limited
ax.w_zaxis.set_ticklabels([])

# not in use until all intensities are added
# ax.w_zaxis.set_ticklabels([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])

ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
ax.set_yticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])  # limited to 10 as no pattern exceeds 10 (yet)

# see reason above
# ax.set_zticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])

ax.set_xlabel('Sensor id')
ax.set_ylabel('Order in pattern')
ax.set_zlabel('Intensity')

ax.set_title("KMeans " + str(len(labels)) + " clusters")
ax.dist = 12

labelData = {};

for l in labels:
    labelData[l] = {
        's': 0,
        'o': 0,
        'i': 0,
        'a': 0
    };

for subD in data:
    labelData[labels[subD[3]]]['s'] += subD[0]
    labelData[labels[subD[3]]]['o'] += subD[1]
    labelData[labels[subD[3]]]['i'] += subD[2]
    labelData[labels[subD[3]]]['a'] += 1

for l in labels:
    # Possible removed patterns by destinct
    if labelData[l]['a'] == 0:
        continue

    ax.text3D(labelData[l]['s'] / labelData[l]['a'],
              labelData[l]['o'] / labelData[l]['a'],
              labelData[l]['i'] / labelData[l]['a'], l,
              horizontalalignment='center',
              bbox=dict(alpha=.2, edgecolor='w', facecolor='w'))

# Show fancy plot
plt.show()

# Prediction results in text
print("\n\n")

a = []
for i in range(0, len(labels)):
    a.append(0)

for li, ln in enumerate(labels):
    pattern_data = PatternData.get_pattern(ln)

    if not pattern_data:
        print(ln + " was removed in distinct process\n\n")
        continue

    estimations = pattern_estimator.predict(pattern_data)

    for x in estimations:
        a[x] += 1

    print("predicting " + ln + " results in ")

    for label_i, amount in enumerate(a):
        if amount > 0:
            print(("%.2f - " +  labels[label_i]) % (100 / len(estimations) * amount))

    a = []
    for i in range(0, len(labels)):
        a.append(0)

    print("\n")